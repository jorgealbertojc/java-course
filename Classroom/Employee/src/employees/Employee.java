package employees;

import java.util.Date;

public class Employee {
	private Integer id;
	private String firstName;
	private String lastName;
	private Integer age;
	private String email;
	private String phoneNumber;
	private double salary;
	private Job job;
	private Date hireDate;
	private String department;

	public Employee(String firstName, String lastName, Integer age,
			String email, String phoneNumber, double salary, Job job) {
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setAge(age);
		this.setEmal(email);
		this.setPhoneNumber(phoneNumber);
		this.setJob(job);
		this.setSalary(salary);
	}

//	public Employee(String firstName, String lastName, String email,
//			String phoneNumber, Date hireDate, Job job, String department) {
//		this.setFirstName(firstName);
//		this.setLastName(lastName);
//		this.setEmal(email);
//		this.setPhoneNumber(phoneNumber);
//		this.setHireDate(hireDate);
//		this.setJob(job);
//		this.setDepartment(department);
//		
//		if
//		
//	}

	public Employee(Integer id) {
		this.setId(id);
	}

	public Employee() {

	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastNAme() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getAge() {
		return this.age;
	}

	public boolean setAge(Integer age) {
		boolean returnValue = false;
		if (age > 18) {
			this.age = age;
			returnValue = true;
		}
		return returnValue;
	}

	public String getEmal() {
		return this.email;
	}

	public void setEmal(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public double getSalary() {
		return this.salary;
	}

	public void setSalary(double salary) {
		if ( job.getMaxSalary() < salary ) {
			this.salary = this.getJob().getMaxSalary();
		} else if ( job.getMinSalary() > salary ) {
			this.salary = this.getJob().getMinSalary();
		} else {
			this.salary = salary;
		}
	}

	public Job getJob() {
		return this.job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Date getHireDate() {
		return this.hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
}
