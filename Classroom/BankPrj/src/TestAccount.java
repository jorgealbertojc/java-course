
public class TestAccount {
	
	public static void main (String [] args) {
		
		Account balance = new Account(100);
		System.out.println("El balance actual es: " + balance.getBalance());
		
		balance.deposit(50);
		System.out.println("El balance actual es: " + balance.getBalance());
		
		balance.withdraw(147);
		System.out.println("El balance actual es: " + balance.getBalance());
		
	}
	
}
