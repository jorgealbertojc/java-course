package primitives;

public class Assign {

    public static void main(String args[]) {
        int x, y;
        double z = 3.414;
        double w = 3.1415F;
        boolean truth = true;
        char c;
        String str;
        String str1 = "bye";    
        c = "A";
        str = "Hi out there!";
        x = 6;
        y = 1000;
        y = 3.1415926;
        w = 175,000;        
        truth = 1;
        z = 3.1415926;
        System.out.println("x=" + x + ", y=" + y + ", is=" + truth);
    }
}
